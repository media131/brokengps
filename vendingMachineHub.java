//I am using bits of my work from last year. Two assignments I had to do were an
//assignment that had me code a vending machine that gave back change in 
//increments of 5 cents, so I had to change it to include money input and
//change it to include pennies as well. The other assignment was a project where
//I designed a program to offer 6 games through a menu which I changed to the 5
//vending machine options.

//When i first thought I finished the assignment I did not read the directions fully.
//What i messed up was the direction to input money before making the selection
//The second mistake I made was not having the option to add more products
//It was a major dissapointment from not reading the instructions fully, but i
//finally finished the assignment with a couple old parts which were kept in 
//because I didn't have the mental capacity at the time to find a proper way of doing it

//My algorithm is first having a while loop that would plays while choice number isn't 6
//in that loop i have totalMoneyDesposited (which was misspelled way too late into the program)
//which is initialized by menuAndMoneyInput(), in menuAndMoneyInput I display the options and prices
//also it has a while loop and if/else statement in a do while loop
//the while loop makes sure the user only enters a coin or dollar value, the if/else either displays
//a message when they enter a money value or 0, the if/else have a statement to update the total money deposited
//the do while loop, that has the while loop and if else, plays through when user enters a value
//the loop ends when the user enters 0 then menuAndMoneyInput gives the total money deposited value back
//to a double value which is back in the first while loop with the choice number.
//inside that while loop is a do while loop that plays until the choicenumber is 0
//inside that is a switch case with options 0,1,2,3,4,5,6
//options 1-5 each update the totalcost until 0 or 6 is pressed
//option 6 refunds your initial money and goes back to the beginning
//option 0 subtracts the total cost with your total money deposited then brings you back to the beginning
//if you wish to start over/buy more
import java.util.Scanner;
public class vendingMachineHub
{
	public static void main(String [] args) throws InterruptedException
	{
		double totalMoneyDesposited = 0;
		int choiceNumber = 0;
		
		//System.out.println("You have $"+totalMoneyDesposited);
		
		while(choiceNumber != 6) //play while choice number isn't the cancel number
		{
			totalMoneyDesposited = menuAndMoneyInput();
			printMenuChoices();
			double totalCost =0;
			
			do
	    	{
	    		choiceNumber = readChoiceNumber();
	    		//System.out.println("You have chosen choice #"+choiceNumber); //tracing
	    		switch (choiceNumber)
	    		{
	    		case 1:
	    			//System.out.println("This is option 1."); //For tracing
	    			//System.out.println("Please deposit atleast $0.95."); old code
	    			totalCost=totalCost+.95;
	    			System.out.println("The total cost is $"+totalCost);
	    			System.out.println("Please enter another item, press 0 to pay for items, ");
	    			System.out.println("or press 6 to refund your payment.");
	    			System.out.println("");
	    			break;
	    		case 2:
	    			//System.out.println("This is option 2."); //For tracing
	    			//System.out.println("Please deposit atleast $1.25."); old code
	    			totalCost=totalCost+1.25;
	    			System.out.println("The total cost is $"+totalCost);
	    			System.out.println("Please enter another item, press 0 to pay for items, ");
	    			System.out.println("or press 6 to refund your payment.");
	    			System.out.println("");
	    			break;
	    		case 3:
	    			//System.out.println("This is option 3."); //For tracing
	    			//System.out.println("Please deposit atleast $1.00."); //old code
	    			totalCost=totalCost+1.00;
	    			System.out.println("The total cost is $"+totalCost);
	    			System.out.println("Please enter another item, press 0 to pay for items, ");
	    			System.out.println("or press 6 to refund your payment.");
	    			System.out.println("");
	    			break;
	    		case 4:
	    			//System.out.println("This is option 4."); //For tracing
	    			//System.out.println("Please deposit atleast $1.50."); //old code
	    			totalCost=totalCost+1.50;
	    			System.out.println("The total cost is $"+totalCost);
	    			System.out.println("Please enter another item, press 0 to pay for items, ");
	    			System.out.println("or press 6 to refund your payment.");
	    			System.out.println("");
	    			break;
	    		case 5:
	    			//System.out.println("This is option 5."); //For tracing
	    			//System.out.println("Please deposit atleast $1.15."); //old code
	    			totalCost=totalCost+1.15;
	    			System.out.println("The total cost is $"+totalCost);
	    			System.out.println("Please enter another item, press 0 to pay for items, ");
					System.out.println("or press 6 to refund your payment.");
					System.out.println("");
					break;			
	    		case 6:
	    			choiceNumber = 0;
	    			totalCost=0;
	    			totalMoneyDesposited = totalMoneyDesposited*100; //times it by one hundred so it works with my vending machine which uses int
	    			System.out.println("You have pressed the refund button.");
	    			System.out.println("");
	    			int quarter5, dime5, nickel5, penny5;
	    			int amount5 = (int)totalMoneyDesposited;
		        	quarter5 = amount5 / 25;
		        	amount5 = amount5 % 25;
		        	dime5 = amount5 / 10;
		        	amount5 = amount5 % 10;
		        	nickel5 = amount5 / 5;
		        	amount5 = amount5 % 5;
		        	penny5 = amount5 / 1;
		        	amount5 = amount5 % 1;
		        	System.out.println("Your change is");
		        	System.out.println(quarter5 + " quarters");
		        	System.out.println(dime5 + " dimes");
		        	System.out.println(nickel5 + " nickels");
		        	System.out.println(penny5 + " pennies");
		        	System.out.println("");
		        	System.out.println("You will be returned to the options and prices menu.");
		        	break;
	    		case 0:
					System.out.println("Selection Finished."); //For tracing
					choiceNumber = 0;
					if (totalMoneyDesposited<totalCost)
		    		{
		    			System.out.println("Insufficient funds");
		    			System.out.println("Please choose something within your price range.");
		    			totalCost=0;
		    			totalMoneyDesposited = totalMoneyDesposited*100; //times it by one hundred so it works with my vending machine which uses int
		    			int quarter2, dime2, nickel2, penny2;
		    			int amount2 = (int)totalMoneyDesposited;
			        	quarter2 = amount2 / 25;
			        	amount2 = amount2 % 25;
			        	dime2 = amount2 / 10;
			        	amount2 = amount2 % 10;
			        	nickel2 = amount2 / 5;
			        	amount2 = amount2 % 5;
			        	penny2 = amount2 / 1;
			        	amount2 = amount2 % 1;
			        	System.out.println("Your change is");
			        	System.out.println(quarter2 + " quarters");
			        	System.out.println(dime2 + " dimes");
			        	System.out.println(nickel2 + " nickels");
			        	System.out.println(penny2 + " pennies");
			        	System.out.println("");
			        	System.out.println("You will be returned to the options and prices menu.");
		    			break;
		    			
		    		}
		    		totalMoneyDesposited = totalMoneyDesposited - (totalCost);
		    		totalMoneyDesposited = totalMoneyDesposited*100;
		    		int quarter, dime, nickel, penny;
		    		int amount = (int)totalMoneyDesposited; //forces money to change to int value
		        	quarter = amount / 25;
		        	amount = amount % 25;
		        	dime = amount / 10;
		        	amount = amount % 10;
		        	nickel = amount / 5;
		        	amount = amount % 5;
		        	penny = amount / 1;
		        	amount = amount % 1;
		        	System.out.println("Here are your items.");
		        	System.out.println("Your change is");
		        	System.out.println(quarter + " quarters");
		        	System.out.println(dime + " dimes");
		        	System.out.println(nickel + " nickels");
		        	System.out.println(penny + " pennies");
		        	System.out.println("");
		        	System.out.println("You will be returned to the options and prices menu.");
					break;
	    		}
			//System.out.println("The total cost is "+totalCost); //tracing
			//System.out.println("choice number is "+choiceNumber); //tracing
	    	} while(choiceNumber!=0);			
		}
	}
	
	private static double menuAndMoneyInput()
	{
		System.out.println("");
		System.out.println("These are the options and prices\n"
				+ "\n"
				+ "Tasty Fruit  : $0.95\n"
				+ "Candy Bar    : $1.25\n"
				+ "Salty Chips  : $1.00\n"
				+ "Soda Drink   : $1.50\n"
				+ "Water Bottle : $1.15");		
    	System.out.println("");
    	System.out.println("Please deposit your money in coins or one dollar or five dollar bills.");
    	System.out.println("(.01, .05, .1, .25, 1, and 5 only accepted.)");
    	System.out.println("");
    	
    	Scanner keyboard = new Scanner(System.in);
    	double moneyDeposited = 0, totalMoneyDesposited = 0;
    	do
    	{
    		moneyDeposited = keyboard.nextDouble();
    		//System.out.println("You have deposited $"+moneyDeposited); //tracing
    		while(moneyDeposited !=0 && moneyDeposited !=.01 && moneyDeposited !=.05 && moneyDeposited !=.10 && moneyDeposited !=.25 && moneyDeposited !=1 && moneyDeposited !=5)
    		{
    			System.out.println("Please deposit a penny, dime, nickel, quarter, one dollar bill, or five dollar bill");
    			System.out.println("You have a total of $"+totalMoneyDesposited+" deposited");
    			System.out.println("");
    			moneyDeposited = keyboard.nextDouble();
    		};
    		
    		if (moneyDeposited!=0)
    		{
    			System.out.println("You have deposited $" +moneyDeposited);
    			totalMoneyDesposited = totalMoneyDesposited += moneyDeposited;
    			System.out.println("You have a total of $"+totalMoneyDesposited+" deposited");
    			System.out.println("");
    		}
    		else
    		{
    			System.out.println("Which totals your payment to $"+totalMoneyDesposited);
    			totalMoneyDesposited = totalMoneyDesposited += moneyDeposited;
    			System.out.println("");
    		}
    		//totalMoneyDesposited = totalMoneyDesposited += moneyDeposited; //tracing    		
    		//System.out.println("totalMoneyDesposited "+ totalMoneyDesposited); //tracing
    	} while(moneyDeposited!=0);
    	return totalMoneyDesposited;
	}
	
	private static void printMenuChoices() //displays prices and numbers for selection
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Press enter to continue");
		keyboard.nextLine();
		System.out.println(""+
	      "                        Choose an option.\n"
		+ "                        1) Tasty Fruit  : $0.95 \n"
		+ "                        2) Candy Bar    : $1.25 \n"
		+ "                        3) Salty Chips  : $1.00 \n"
		+ "                        4) Soda Drink   : $1.50 \n"
		+ "                        5) Water Bottle : $1.15 \n"
		+ "                        6) Cancel and Refund money. \n"
		+ "                        0) Confirm Selections. \n");

	}
	
	private static int readChoiceNumber() //reads your choice number
	{
		Scanner keyboard = new Scanner(System.in);
		int choiceNumber;
		choiceNumber = keyboard.nextInt();
		while(choiceNumber < 0 || choiceNumber > 6) //limits user to only click 0-6
		{
			System.out.println("                       This is not a valid number");
			System.out.println("                       Try a number between 0 through 6");
			choiceNumber = keyboard.nextInt();
		}
		return choiceNumber;
	}
}